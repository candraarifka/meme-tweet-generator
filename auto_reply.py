import tweepy
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import memegenerator
import re
import time 

CONSUMER_KEY = ''
CONSUMER_SECRET = ''
ACCESS_KEY = ''
ACCESS_SECRET = ''
auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)
api = tweepy.API(auth)


class CustomStreamListener(StreamListener):
    def on_status(self, status, count=1):
        
        ###clean up username prefix '@'
        ###create meme using that status text
        ###limit stream status using time
        ###create tweet by meme created
        status_text = re.sub('@[^\s]+','', status.text)
        memegenerator2.make_meme(status_text, " by @"+status.user.screen_name)
        output = 'temp.png'
        try:
            status = api.update_with_media(output, " by @"+status.user.screen_name)
            api.update_status(status=status,  in_reply_to_status_id = status.id)
        except:
            time.sleep(5)

    def on_error(self, status_code):
        print (sys.stderr + ' Encountered error with status code: ' + status_code)
        return True # Don't kill the stream

    def on_timeout(self):
        print (sys.stderr + 'Timeout...')
        return True # Don't kill the stream


###stream tweet
###take user detail status by username
sapi = Stream(auth, CustomStreamListener())
target_user = ''
sapi.filter(track=[target_user])

